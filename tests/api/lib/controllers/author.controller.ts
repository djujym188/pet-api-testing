import { ApiRequest } from "./request";

export class AuthorController {
    async postLogin() {
        const response = await new ApiRequest()
            .prefixUrl("https://knewless.tk/api/")
            .method("POST")
            .url(`auth/login`)
            .body({ "email": "testcasesjs@gmail.com", "password":"weather18"} )
            .send();
        return response;
    }

    async getAuthor(bearerToken){
        const response = await new ApiRequest()
            .prefixUrl("https://knewless.tk/api/")
            .method("GET")
            .url(`author`)
            .bearerToken(bearerToken)
            .send();
        return response;
    }

    async postAuthor(bearerToken, Id, userId) {
        const response = await new ApiRequest()
            .prefixUrl("https://knewless.tk/api/")
            .method("POST")
            .url(`author`) 
            .bearerToken(bearerToken)
            .body({ 
                "avatar": null,
                "biography": "",
                "company": null,
                "firstName": "sss",
                "id": Id,
                "job": null,
                "lastName": "sss",
                "location": "Aruba",
                "twitter": "https://twitter.com/",
                "userId": userId,
                "website": ""
            })
            .send();
        return response;
    }

 
    async getUserMe(bearerToken) {
        const response = await new ApiRequest()
            .prefixUrl("https://knewless.tk/api/")
            .method("GET")
            .url(`user/me`) 
            .bearerToken(bearerToken)
            .send();
        return response;
    }


     async getAuthorOverviewId(bearerToken, Id) {
        const response = await new ApiRequest()
            .prefixUrl("https://knewless.tk/api/")
            .method("GET")
            .url("author/overview/"+Id) 
            .bearerToken(bearerToken)
            .send();
        return response;
    }

    async postArticle(bearerToken, Id,) {
        const response = await new ApiRequest()
            .prefixUrl("https://knewless.tk/api/")
            .method("POST")
            .url(`article`) 
            .bearerToken(bearerToken)
            .body({ 
                "articleId": "articleId",
                "id": Id,
                "text": "string",
            })
            .send();
        return response;
    }

    async getArticleAuthor(bearerToken) {
        const response = await new ApiRequest()
            .prefixUrl("https://knewless.tk/api/")
            .method("GET")
            .url(`article/author`) 
            .bearerToken(bearerToken)
            .send();
        return response;
    }

    async getArticleArticleId(bearerToken,articleId)  {
        const response = await new ApiRequest()
            .prefixUrl("https://knewless.tk/api/")
            .method("GET")
            .url(`article/`+ articleId) 
            .bearerToken(bearerToken)
            .send();
        return response;
    }

    async postArticleComment(bearerToken, Id, articleId)  {
        const response = await new ApiRequest()
            .prefixUrl("https://knewless.tk/api/")
            .method("POST")
            .url(`article_comment`) 
            .bearerToken(bearerToken)
            .body({ 
                "articleId": articleId,
                "id": Id,
                "text": "string",
            })
            .send();
        return response;
    }
}



