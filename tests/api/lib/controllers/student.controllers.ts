import { ApiRequest } from "./request";

export class StudentController {
    async postLogin() {
        const response = await new ApiRequest()
            .prefixUrl("https://knewless.tk/api/")
            .method("POST")
            .url(`auth/login`)
            .body({ "email": "djujym188@gmail.com", "password":"weather18"} )
            .send();
        return response;
    }

    async getStudent(bearerToken){
        const response = await new ApiRequest()
            .prefixUrl("https://knewless.tk/api/")
            .method("GET")
            .url(`student`)
            .bearerToken(bearerToken)
            .send();
        return response;
    }

    async postStudent(bearerToken, Id, userId) {
        const response = await new ApiRequest()
            .prefixUrl("https://knewless.tk/api/")
            .method("POST")
            .url(`student/`) 
            .bearerToken(bearerToken)
            .body({ 
                "avatar": null,
                "biography": "qa",
                "company": "",
                "direction": "Developer",
                "education": "High School",
                "employment": "Employee",
                "experience": 0,
                "firstName": "Anna",
                "id": Id,
                "industry": "Web Services",
                "job": "",
                "lastName": "Rrrr",
                "level": "Beginner",
                "location": "Antigua and Barbuda",
                "role": "UI/UX Designer",
                "tags": [
                  {
                    "id":Id,
                    "imageSrc": "",
                    "name": "Anna"
                  }
                ],
                "userId": userId,
                "website": "",
                "year": 1950
            })
            .send();
        return response;
    }

 
    async getUserMe(bearerToken) {
        const response = await new ApiRequest()
            .prefixUrl("https://knewless.tk/api/")
            .method("GET")
            .url(`user/me`) 
            .bearerToken(bearerToken)
            .send();
        return response;
    }


     async getCourseAll(bearerToken) {
        const response = await new ApiRequest()
            .prefixUrl("https://knewless.tk/api/")
            .method("GET")
            .url(`course/all`) 
            .bearerToken(bearerToken)
            .send();
        return response;
    }
}