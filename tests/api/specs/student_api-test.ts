import { expect } from "chai";
import { stringify } from "querystring";
import { StudentController} from "../lib/controllers/student.controllers"
const student = new StudentController();
let bearerToken: string;
let userId: string;
let Id: string;

describe("Student controller", () => {
    it('test 1', async() =>{
        let response = await student.postLogin()
        console.log(response.body);

        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
        expect(response.timings.phases.total, `Response time should be less than 5s`).to.be.lessThan(5000);
        
        bearerToken = response.body.accessToken;
    })

    it('test 2', async() =>{
        let response = await student.getStudent(bearerToken)
        console.log(response.body);
       

       expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
       expect(response.timings.phases.total, `Response time should be less than 5s`).to.be.lessThan(5000);
      
       Id = response.body.Id;
       userId = response.body.userId;

        
    })

    it('test 3', async() =>{
        let response = await student.postStudent(bearerToken, Id, userId )
        console.log(response.body);

        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
        expect(response.timings.phases.total, `Response time should be less than 5s`).to.be.lessThan(5000);
       
    })


    it('test 4', async() =>{
        let response = await student.getUserMe(bearerToken)
        console.log(response.body);

        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
        expect(response.timings.phases.total, `Response time should be less than 5s`).to.be.lessThan(5000);
        
    })


    it('test 5', async() =>{
        let response = await student.getCourseAll(bearerToken)
        console.log(response.body);
       

        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
        expect(response.timings.phases.total, `Response time should be less than 5s`).to.be.lessThan(5000);
       
    })

})