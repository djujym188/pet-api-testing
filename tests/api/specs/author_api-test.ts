import { expect } from "chai";
import { stringify } from "querystring";
import { AuthorController} from "../lib/controllers/author.controller"
const author = new AuthorController();
let bearerToken: string;
let userId: string;
let Id: string;
let articleId: string;

describe("Author controller", () => {
    it('test 1', async() =>{
        let response = await author.postLogin()
        console.log(response.body);

        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
        expect(response.timings.phases.total, `Response time should be less than 5s`).to.be.lessThan(5000);
        
        bearerToken = response.body.accessToken;
    })

    it('test 2', async() =>{
        let response = await author.getAuthor(bearerToken)
        console.log(response.body);
       

       expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
       expect(response.timings.phases.total, `Response time should be less than 5s`).to.be.lessThan(5000);
      
       Id = response.body.Id;
       userId = response.body.userId;

        
    })

    it('test 3', async() =>{
        let response = await author.postAuthor(bearerToken, Id, userId )
        console.log(response.body);

        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
        expect(response.timings.phases.total, `Response time should be less than 5s`).to.be.lessThan(5000);
       
    })


    it('test 4', async() =>{
        let response = await author.getUserMe(bearerToken)
        console.log(response.body);

        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
        expect(response.timings.phases.total, `Response time should be less than 5s`).to.be.lessThan(5000);
        
    })


    it('test 5', async() =>{
        let response = await author.getAuthorOverviewId(bearerToken, Id)
        console.log(response.body);
       

        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
        expect(response.timings.phases.total, `Response time should be less than 5s`).to.be.lessThan(5000);
       
    })

    it('test 6', async() =>{
        let response = await author.postArticle(bearerToken, Id)
        console.log(response.body);

        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
        expect(response.timings.phases.total, `Response time should be less than 5s`).to.be.lessThan(5000);
       articleId = response.body.articleId
    }) 

    it('test 7', async() =>{
        let response = await author.getArticleAuthor(bearerToken )
        console.log(response.body);

        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
        expect(response.timings.phases.total, `Response time should be less than 5s`).to.be.lessThan(5000);
       
    })

    it('test 8', async() =>{

        let response = await author.getArticleArticleId(bearerToken,articleId )
        console.log(response.body);

        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
        expect(response.timings.phases.total, `Response time should be less than 5s`).to.be.lessThan(5000);
       
    })

    it('test 9', async() =>{
        let response = await author.postArticleComment(bearerToken,Id, articleId)
        console.log(response.body);

        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
        expect(response.timings.phases.total, `Response time should be less than 5s`).to.be.lessThan(5000);
       
    })




})